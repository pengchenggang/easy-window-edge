// 规则：台式机配置在台式机改完，然后同步到笔记本程序目录
// computerListRight 右侧列表
// computerListBottom 右下角
// 左侧竖着的列表
window.computerList = [
  {
    title: 'Nginx Reload',
    describe: '重启nginx-管理员权限',
    icon: 'nginx.png',
    exePath: String.raw`D:\softz\innerComputerSoft\nginx-1.15.10-using\nginx.exe -s reload.bat`,
    param: '',
  },
  {
    title: 'Nginx Start',
    describe: '启动nginx-管理员权限',
    icon: 'nginx.png',
    exePath: String.raw`D:\softz\innerComputerSoft\nginx-1.15.10-using\start.bat`,
    param: '',
  },
  {
    title: 'Nginx Stop',
    describe: '停止nginx-管理员权限',
    icon: 'nginx.png',
    exePath: String.raw`D:\softz\innerComputerSoft\nginx-1.15.10-using\stop.bat`,
    param: '',
  },
  {
    title: '双人网-语音',
    describe: '修改网关-管理员权限',
    icon: 'bat.png',
    exePath: String.raw`C:\Users\LENOVO\Desktop\语音系统-网关-ip修改-172.16.40.155.bat`,
    param: '',
  },
  {
    title: '公司内网',
    describe: '修改网关-管理员权限',
    icon: 'bat.png',
    exePath: String.raw`C:\Users\LENOVO\Desktop\公司内网-网关-ip修改-192.168.42.142 - 副本.bat`,
    param: '',
  },
  {
    title: '平台-vscode-内',
    describe: '平台项目代码',
    icon: 'vscode.png',
    exePath: String.raw`C:\Users\LENOVO\AppData\Local\Programs\Microsoft VS Code\Code.exe`,
    param: String.raw`D:\projects\aizzb_web`,
  },
  {
    title: '单点登录-vscode-内',
    describe: '单点登录项目代码',
    icon: 'vscode.png',
    exePath: String.raw`C:\Users\LENOVO\AppData\Local\Programs\Microsoft VS Code\Code.exe`,
    param: String.raw`D:\projects\aizzb_single_login`,
  },
]

// 右侧图标列表
window.computerListRight = [
  {
    title: '小火柴-配置目录(台式机)',
    icon: 'folder.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`D:\aardio-soft\easy-window-edge\web`
  },
  {
    title: 'ComputerList.js',
    describe: '平台项目代码',
    icon: 'vscode.png',
    exePath: String.raw`C:\Users\LENOVO\AppData\Local\Programs\Microsoft VS Code\Code.exe`,
    param: String.raw`D:\aardio-soft\easy-window-edge\web`,
  },
  {
    title: 'Quick_Any2Ico',
    icon: 'Quick_Any2Ico.png',
    exePath: String.raw`D:\aardio-soft\Quick_Any2Ico\Quick_Any2Ico.exe`,
    param: String.raw``
  },
  {
    title: 'icons',
    icon: 'folder.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`D:\aardio-soft\easy-window-edge\web\images\icons`
  },
  {
    title: 'projects 项目列表',
    icon: 'folder.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`D:\projects`
  },
  {
    title: 'Chrome',
    icon: 'Chrome.png',
    exePath: String.raw`C:\Program Files\Google\Chrome\Application\chrome.exe`,
    param: String.raw``
  },
  {
    title: 'firefox',
    icon: 'firefox.png',
    exePath: String.raw`C:\Program Files\Mozilla Firefox\firefox.exe`,
    param: String.raw``
  },
  {
    title: 'Edge',
    icon: 'msedge.png',
    exePath: String.raw`C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe`,
    param: String.raw``
  },
  {
    title: 'crypto-js-des（本机）',
    icon: 'Chrome.png',
    exePath: String.raw`C:\Program Files\Google\Chrome\Application\chrome.exe`,
    param: String.raw`D:\project-tools\crypto-js-des\index.html`
  },
  {
    title: 'Git Bash',
    icon: 'git-bash.png',
    exePath: String.raw`C:\Program Files\Git\git-bash.exe`,
    param: String.raw``
  },
  {
    title: 'HotkeyP',
    icon: 'HotkeyP.png',
    exePath: String.raw`D:\softz\hotkeyp\HotkeyP.exe`,
    param: String.raw``
  },
  {
    title: 'MobaXterm1_CHS1',
    icon: 'MobaXterm.png',
    exePath: String.raw`D:\softz\MobaXtermch_jb51\MobaXterm_20.0汉化\MobaXterm1_CHS1.exe`,
    param: String.raw``
  },
  {
    title: 'notepad++（记事本）',
    icon: 'notepad++.png',
    exePath: String.raw`C:\Program Files\Notepad++\notepad++.exe`,
    param: String.raw`D:\workNote\记事本.txt`
  },
  {
    title: 'Snipaste',
    icon: 'Snipaste.png',
    exePath: String.raw`D:\softz\Snipaste2.7.3\Snipaste-2.7.3-Beta-x64\Snipaste.exe`,
    param: String.raw``
  },
  {
    title: 'WPS Office',
    icon: 'wps.png',
    exePath: String.raw`C:\Users\LENOVO\AppData\Local\Kingsoft\WPS Office\ksolaunch.exe`,
    param: String.raw``
  },
  {
    title: '内网通',
    icon: 'ShiYeLine.png',
    exePath: String.raw`C:\Program Files (x86)\Nwt\ShiYeLine.exe`,
    param: String.raw`xuaichen`
  },
  {
    title: '编办系统运维(network-management)',
    titleTip: true,
    icon: 'vscode.png',
    exePath: String.raw`C:\Users\LENOVO\AppData\Local\Programs\Microsoft VS Code\Code.exe`,
    param: String.raw`D:\projects\network-management`,
  },
  {
    type: 'neiwangtong',
    neiwangtongName: 'xuaichen',
    title: '徐爱臣',
    icon: 'ShiYeLine.png',
  },
  {
    type: 'neiwangtong',
    neiwangtongName: '李杰',
    title: '李杰',
    icon: 'ShiYeLine.png',
  },
]

// 右下角小图标
window.computerListBottom = [
  {
    title: 'Draw.io',
    icon: 'draw.io.png',
    exePath: String.raw`C:\Program Files\draw.io\draw.io.exe`,
    param: '',
  },
  {
    title: 'IDEA-2023(12380)',
    icon: 'idea.png',
    exePath: String.raw`C:\Program Files\JetBrains\IntelliJ IDEA 2023.2.5\bin\idea64.exe`,
    param: '',
  },
  {
    title: 'Nginx目录',
    icon: 'nginx.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`D:\softz\innerComputerSoft\nginx-1.15.10-using`,
  },
]