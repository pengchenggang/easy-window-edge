// laptopListRight 右侧列表
// laptopListBottom 右下角
// 左侧竖着的列表
window.laptopList = [
  {
    title: 'desktopz',
    describe: '之前的桌面整理 慢慢整理吧',
    icon: 'folder.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`E:\desktopz`
  },
  {
    title: 'Nginx Reload',
    describe: '重启nginx',
    icon: 'nginx.png',
    exePath: String.raw`D:\softz\innerComputerSoft\nginx-1.15.10-using\nginx.exe -s reload.bat`,
    param: '',
  },
  {
    title: 'Nginx Start',
    describe: '启动nginx',
    icon: 'nginx.png',
    exePath: String.raw`D:\softz\innerComputerSoft\nginx-1.15.10-using\start.bat`,
    param: '',
  },
  {
    title: 'Nginx Stop',
    describe: '停止nginx',
    icon: 'nginx.png',
    exePath: String.raw`D:\softz\innerComputerSoft\nginx-1.15.10-using\stop.bat`,
    param: '',
  },
  {
    title: '平台-vscode',
    describe: '平台项目',
    icon: 'vscode.png',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Programs\Microsoft VS Code\Code.exe`,
    param: String.raw`C:\project\aizzb_project_web`
  },
]

// 右侧图标列表
window.laptopListRight = [
  {
    title: '火柴桌面-配置目录(笔记本)',
    icon: 'folder.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`D:\aardio-projects\easy-window-edge\dist\web`
  },
  {
    title: 'LaptopList.js',
    icon: 'vscode.png',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Programs\Microsoft VS Code\Code.exe`,
    param: String.raw`D:\aardio-projects\easy-window-edge\dist\web`,
  },
  {
    title: 'aardio源码搜索',
    icon: 'vscode.png',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Programs\Microsoft VS Code\Code.exe`,
    param: String.raw`C:\GreenSoft\aardio`,
  },
  {
    title: 'PhotoShop 2014',
    icon: 'photoshop.png',
    describe: 'ps',
    exePath: String.raw`C:\Program Files\Adobe\Adobe Photoshop CC 2014\Photoshop.exe`,
    param: String.raw``
  },
  {
    title: 'MobaXterm',
    icon: 'MobaXterm.png',
    exePath: String.raw`C:\Program Files (x86)\Mobatek\MobaXterm\MobaXterm.exe`,
    param: String.raw``
  },
  {
    title: 'Quick_Any2Ico',
    icon: 'Quick_Any2Ico.png',
    exePath: String.raw`D:\aardio-projects\easy-window-edge\Quick_Any2Ico.exe`,
    param: String.raw``
  },
  {
    title: 'icons',
    icon: 'folder.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`D:\aardio-projects\easy-window-edge\dist\web\images\icons`
  },
  {
    title: 'nvm-desktop 切换nodejs',
    icon: 'nvm-desktop.png',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Programs\nvm-desktop\nvm-desktop.exe`,
    param: String.raw``
  },
  {
    title: '图标制作 Greenfish Icon Editor Pro 3.5',
    titleTip: true,
    icon: 'gfie.png',
    exePath: String.raw`C:\Program Files\Greenfish Icon Editor Pro 3.5\gfie.exe`,
    param: String.raw``
  },
  {
    title: 'HotkeyP',
    icon: 'HotkeyP.png',
    exePath: String.raw`C:\GreenSoft\hotkeyp\HotkeyP.exe`,
    param: String.raw``
  },
  {
    title: 'AxureRP9 原型设计',
    icon: 'AxureRP9.png',
    exePath: String.raw`C:\Program Files (x86)\Axure\Axure RP 9\AxureRP9.exe`,
    param: String.raw``
  },
  {
    title: 'Illustrator',
    icon: 'Illustrator.png',
    exePath: String.raw`C:\Program Files\Adobe\Adobe Illustrator CC 2014\Support Files\Contents\Windows\Illustrator.exe`,
    param: String.raw``
  },
  {
    title: 'Navigator',
    icon: 'vscode.png',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Programs\Microsoft VS Code\Code.exe`,
    param: String.raw`C:\project\navigator`,
  },
  {
    title: '幕布',
    icon: 'mubu.png',
    exePath: String.raw`C:\Program Files (x86)\Mubu\幕布.exe`,
    param: String.raw``,
  },
  {
    title: '微信',
    icon: 'WeChat.png',
    exePath: String.raw`C:\Program Files (x86)\Tencent\WeChat\WeChat.exe`,
    param: String.raw``,
  },
  {
    title: '今日亦是欧皇的一日',
    weixinName: '今日亦是欧皇的一日',
    icon: 'WeChat.png',
    type: 'weixin',
  },
  {
    title: '徐爱臣',
    weixinName: '徐爱臣',
    icon: 'WeChat.png',
    type: 'weixin',
  },
  {
    title: '洛雪音乐 LX-music',
    icon: 'lx-music-desktop.png',
    describe: 'yinyue',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Programs\lx-music-desktop\lx-music-desktop.exe`,
    param: String.raw``,
  },
  {
    title: 'Chrome',
    icon: 'Chrome.png',
    exePath: String.raw`C:\Program Files\Google\Chrome\Application\chrome.exe`,
    param: String.raw``
  },
  {
    title: '火狐52版-国产机',
    icon: 'firefox.png',
    describe: '',
    exePath: String.raw`C:\Program Files\Mozilla Firefox\firefox.exe`,
    param: String.raw``,
  },
  {
    title: 'Edge',
    icon: 'msedge.png',
    describe: '',
    exePath: String.raw`C:\Program Files (x86)\Microsoft\Edge Dev\Application\msedge.exe`,
    param: String.raw``,
  },
  {
    title: '光速英语-单词3000(41-60)收费',
    icon: 'folder2.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`E:\baidu_download\光速英语\3[C]光速单词3000(第41-60节)`
  },
  {
    title: 'Logseq 笔记本(记事本)',
    icon: 'Logseq.png',
    describe: '',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Logseq\Logseq.exe`,
    param: String.raw``,
  },
  {
    title: 'Balsamiq Wire frames',
    icon: 'BalsamiqWireframes.png',
    describe: '',
    exePath: String.raw`C:\Program Files\Balsamiq\Balsamiq Wireframes\BalsamiqWireframes.exe`,
    param: String.raw``,
  },
  {
    title: 'flomo卡片笔记(记事本)',
    icon: 'flomo.png',
    describe: '',
    exePath: String.raw`C:\Program Files\flomo\flomo.exe`,
    param: String.raw``,
  },
  {
    title: 'Joplin笔记(记事本)',
    icon: 'Joplin.png',
    describe: '',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Programs\Joplin\Joplin.exe`,
    param: String.raw``,
  },
  {
    title: 'Obsidian笔记(记事本)',
    icon: 'Obsidian.png',
    describe: '',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Obsidian\Obsidian.exe`,
    param: String.raw``,
  },
  {
    title: 'Snipaste截图',
    icon: 'Snipaste.png',
    describe: '',
    exePath: String.raw`E:\myHome\rolanDesk\greenSoft\Snipaste2.7.3\Snipaste-2.7.3-Beta-x64\Snipaste.exe`,
    param: String.raw``,
  },
  {
    title: 'Xmind 思维导图',
    icon: 'Xmind.png',
    describe: '',
    exePath: String.raw`C:\Users\Reciter\AppData\Local\Programs\Xmind\Xmind.exe`,
    param: String.raw``,
  },
  {
    title: 'ONENOTE(记事本)',
    icon: 'ONENOTE.png',
    describe: '',
    exePath: String.raw`C:\Program Files\Microsoft Office\root\Office16\ONENOTE.EXE`,
    param: String.raw``,
  },
  {
    title: 'Typora 0.11.18(记事本)',
    icon: 'Typora.png',
    describe: '',
    exePath: String.raw`C:\Program Files\Typora\Typora.exe`,
    param: String.raw``,
  },
  {
    title: 'MagicMouseTrails 鼠标小尾巴',
    icon: 'MagicMouseTrails_p.png',
    describe: '',
    exePath: String.raw`E:\myHome\rolanDesk\greenSoft\MagicMouse_85769\MagicMouseTrails_p.exe`,
    param: String.raw``,
  },
  {
    title: '拖拽录入',
    icon: 'appIcon.png',
    describe: '',
    type: 'tuozhuailuru'
  },
  {
    title: '抖音直播伴侣',
    icon: 'zhibobanlv.png',
    describe: '',
    exePath: String.raw`C:\Program Files (x86)\webcast_mate\直播伴侣.exe`,
    param: String.raw``,
  },
  {
    title: 'phpstudy_pro',
    icon: 'phpstudy_pro.png',
    describe: '',
    exePath: String.raw`D:\phpstudy_pro\COM\phpstudy_pro.exe`,
    param: String.raw``,
  },
  // bigListInsert
]

// 右下角小图标 40-40
window.laptopListBottom = [
  {
    title: 'Draw.io',
    icon: 'draw.io.png',
    exePath: String.raw`C:\Program Files\draw.io\draw.io.exe`,
    param: '',
  },
  {
    title: 'IDEA-2018(12380)',
    icon: 'idea.png',
    exePath: String.raw`C:\Program Files\JetBrains\IntelliJ IDEA 2018.1.5\bin\idea64.exe`,
    param: '',
  },
  {
    title: 'Nginx',
    icon: 'nginx.png',
    exePath: String.raw`C:\Windows\explorer.exe`,
    param: String.raw`E:\desktopz\02-软件工具（双层目录）\Nginx`,
  },
  {
    title: 'clash',
    icon: 'clash.png',
    exePath: String.raw`E:\myHome\rolanDesk\greenSoft\Clash.for.Windows-0.19.21-ikuuu\Clash.for.Windows-0.19.21-ikuuu\Clash for Windows.exe`,
    param: '',
  }
]
